var login = require("facebook-chat-api"),
  prompt = require('prompt'),
  argv = require('yargs').argv,
  logger = require('./utils/logger'),
  handleMessage = require('./core/handleMessage');

logger.debugLevel = 'INFO';

function parseInput (event) {
  var cmdStr = event.body.trim().toLowerCase();
  logger.log("INFO", "Command: " + cmdStr);
  return cmdStr.split(" ");
}

var handleCommand = function(err, api) {
  if (err) return console.error(err);

  api.setOptions({
    listenEvents: true
  });

  api.listen(function(err, event) {
    if (err) return console.error(err);

    switch (event.type) {
      case "message":
        var input = parseInput(event);
        if (input.indexOf("olaf") > -1) {
          handleMessage(input, event, function(res) {
            api.markAsRead(event.threadID, function(err) {
              if (err) console.log(err);
            });
            api.sendMessage(res, event.threadID);
          });
        }
        break;
      case "event":
        logger.log("INFO", event);
        break;
    }
  });
}

prompt.override = argv;
prompt.start();
prompt.get(['email', {
  name: 'password',
  hidden: true,
  conform: function(value) {
    return true;
  }
}], function(err, result) {
  login({
    email: result.email,
    password: result.password
  }, handleCommand);
});
