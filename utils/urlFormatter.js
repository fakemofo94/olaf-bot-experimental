module.exports.get_url = function(params) {
  var p, url, _i, _len;
  url = "";
  for (_i = 0, _len = params.length; _i < _len; _i++) {
    p = params[_i];
    url += "/" + (encodeURIComponent(p));
  }
  return url;
}
