var ignore = require('./commands/ignore')
  unrecognized = require('./commands/unrecognized'),
  hello = require('./commands/hello'),
  compliment = require('./commands/compliment'),
  insult = require('./commands/insult'),
  help = require('./commands/help')

module.exports = {
  'help': help,
  'hi': hello,
  'compliment': compliment,
  'insult': insult,

  // reserved INTERNAL commands
  'ignore': ignore,
  'unrecognized': unrecognized
}
