var get_url = require('../../utils/urlFormatter').get_url,
  logger = require('../../utils/logger'),
  cheerio = require('cheerio'),
  http = require('http')

logger.debugLevel = 'INFO';

var fuckoff_request = function(params, callback) {
  var service_host = "foaas.com";
  var path;
  if (params.name == null) {
    path = get_url([params.keyword, params.from]);
  } else {
    path = get_url([params.keyword, params.name, params.from]);
  }

  logger.log("INFO", "Requesting insult from: " + service_host + path);
  var options = {
    host: service_host,
    port: 80,
    path: path,
    method: 'GET'
  };
  return http.request(options, function(res) {
    res.setEncoding('utf8');
    return res.on('data', function(data) {
      data = cheerio.load(data)("h1").text();
      if (!data.trim()) {
        return;
      }
      logger.log("INFO", "Insult response: " + data);
      return callback(data);
    });
  }).on('error', function(e) {
    return console.error(e.message);
  }).end();
}

module.exports.execute = function(cmd, event, callback) {
  if (cmd.length == 4) {
    var params = {
      keyword: cmd[2],
      name: cmd[3],
      from: event.senderName.split(" ")[0]
    };
    fuckoff_request(params, function(res) {
      return callback(res, event.threadID);
    });
  } else if (cmd.length >= 3) {
    for (var i = 3; i < cmd.length; i++) {
      cmd[2] += " " + cmd[i];
    }
    var params = {
      keyword: cmd[2],
      name: null,
      from: event.senderName.split(" ")[0]
    };
    fuckoff_request(params, function(res) {
      return callback(res);
    });
  }
}

module.exports.getUsageInfo = function() {
  return  "\"Olaf insult <name>\": olaf will insult <name> \n" +
          "\"Olaf insult <keyword> <name>\": olaf will special insult <name>";
}

module.exports.isInternal = function() {
  return false;
}