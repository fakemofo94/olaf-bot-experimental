module.exports.execute = function(cmd, event, callback) {
  var commands = require('../commands')
  
  var res = "list of commands:\n\n";
  for (var i in commands) {
    if (commands[i].isInternal() == false) {
      console.log("got here " + i);
      res += commands[i].getUsageInfo() + "\n";
    }
  }

  return callback(res)
};

module.exports.getUsageInfo = function() {
  return "\"Olaf help\": get help";
}

module.exports.isInternal = function() {
  return false;
}
