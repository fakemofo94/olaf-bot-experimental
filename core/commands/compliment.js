var get_url = require('../../utils/urlFormatter').get_url,
  logger = require('../../utils/logger'),
  cheerio = require('cheerio'),
  http = require('http')

logger.debugLevel = 'INFO';

var moarlove_request = function(params, callback) {
  var service_host = "toykeeper.net";
  var path = get_url(["programs", "mad", "compliments"]);
  logger.log("INFO", "Requesting compliment from " + service_host + path);
  var options = {
    host: service_host,
    port: 80,
    path: path,
    method: 'GET'
  };
  return http.request(options, function(res) {
    res.setEncoding('utf8');
    return res.on('data', function(data) {
      data = cheerio.load(data)("h3").text();
      if (!data.trim()) {
        return;
      }
      logger.log("INFO", "Compliment response: " + data);
      return callback(params.name + ", " + data);
    });
  }).on('error', function(e) {
    return console.error(e.message);
  }).end();
}

module.exports.execute = function(cmd, event, callback) {
  var res = "";
  if (cmd.length >= 3) {
    for (var i = 3; i < cmd.length; i++) {
      cmd[2] += " " + cmd[i];
    }
    var params = {
      name: cmd[2],
    };
    moarlove_request(params, function(result) {
      res = result;
      return callback(res);
    });
  } else {
    res = "I know I'm amazing, but what about you guys? ;)";
    return callback(res);
  }
}

module.exports.getUsageInfo = function() {
    return  "\"Olaf compliment <name>\": olaf will compliment <name>";
}

module.exports.isInternal = function() {
  return false;
}
