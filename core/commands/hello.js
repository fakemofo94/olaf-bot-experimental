module.exports.execute = function(cmd, event, callback) {
  var response = "sup " + event.senderName.split(" ")[0] + "?";
  return callback(response);
}

module.exports.getUsageInfo = function() {
  return "\"Olaf hi/hey\": say hello to olaf";
}

module.exports.isInternal = function() {
  return false;
}