var commands = require('./commands')

module.exports = function(cmd, event, callback) {
  var res = "",
  keyword = cmd[1];
  
  switch (cmd[0]) {
    case "olaf":
      // proper format starts with "olaf" <keyword> ...
      if (commands[keyword]) {
        res = commands[keyword].execute(cmd, event, callback);
      } else {
        res = commands['ignore'].execute(cmd, event, callback);
      }
    break;
    default:
      res = commands['unrecognized'].execute(cmd, event, callback);
  }

  return res;
}
